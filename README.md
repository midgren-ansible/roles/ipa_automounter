# ipa_automounter role

Automount NFS shares from file servers provisioned using IPA/ldap and
sssd.

## Supported Operating Systems

* Ubuntu 20.04
* Ubuntu 18.04
* RedHat / CentOS 7 and 8
* Fedora (not tested)

## Variables

* `ipa_automount_location`

    Name of the location to use with automount.

    Default value: `default`



### IPA family variables

* `ipa_admin_user`

    The IPA account used to add the device to the domain.

    This value must be provided, no defaults.

* `ipa_admin_passwd`

    Password of the IPA user

    This value must be provided, no defaults.

* `ipa_id_min`

* `ipa_id_max`

### Inventory data

This role depends on that the group `primary_ipa_server` is
defined. If the group contains several entries the first one will be
used.
